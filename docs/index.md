---
title: Welcome
---

### 什么是maven-mydalgen-plugin?
一个厌倦了枯燥编码工作的程序猿，放下节操开发的一套maven插件，让代码交由程序去编写。理想很丰满，现实忒骨感，目前仅限于业务系统DAL层代码的自动构建。

>数据访问层(DAL)：主要是对原始数据(数据库或者文本文件等存放数据的形式)的操作层，而不是指原始数据，也就是说，是对数据的操作，而不是数据库，具体为业务逻辑层或表示层提供数据服务。

在DAL层的开发中MyBatis是一款极具代表性的框架，不仅支持定制化SQL、存储过程以及高级映射，还避免了几乎所有的 JDBC 代码和手动设置参数以及获取结果集，并可以对配置和原生Map使用简单的XML或注解，将接口和 Java 的 POJOs(Plain Old Java Objects,普通的 Java对象)映射成数据库中的记录。

MyBatis在一定程度上提升了程序猿的编码效率与质量，但随着业务的逐渐复杂需要投入不少的时间去维护Mapper XML配置以及相同风格的POJO类或者DAO接口。这是一项令程序猿相当反感的工作，为了从中解脱出来，我们开发了mydalgen插件。开发人员只需关注实际业务所对应的SQL语句，通过插件自动生成MyBatis的Mapper XML配置、POJO类、DAO接口。

该项目并非是从零开始，而是在开源项目[https://github.com/codehaus/middlegen3](https://github.com/codehaus/middlegen3 "middlegen3") 上扩展的一款maven插件，在完成模板文件编辑后可执行命令`mvn mydalgen:mybatis `进行代码的构建

<div class="vid-wrapper">
    <video controls="controls" src="/maven-mydalgen-plugin/res/welcome.mp4"></video>
</div>

### 环境依赖
1. 启用了注解的Spring Maven工程
2. MySQL 5+

### 名词解释
- DO(Data Object)	
数据对象，数据库表映射出的java对象，字段名、字段类型都相互匹配。一个DO对象就是对应数据库中某张表的一条记录。
- DAO(Data Access Object)	
数据访问对象，作为业务逻辑与数据库资源的中间层，实现对数据库的操作方法。通常DAO是与PO(Persistant Object)结合使用的，但为了有更高的辨识度，我们将PO重新定义为DO(Data Object)。


### 关于作者
Edit By [Seer](http://git.oschina.net/smartboot)  
E-mail:[zhengjunweimail@163.com](mailto:zhengjunweimail@163.com)  
QQ:504166636