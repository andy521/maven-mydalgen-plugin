---
title: 源码&下载
category: 小目标
order: 2
---

maven-mydalgen-plugin已开源至码云，欢迎前往下载。

- 源码地址：[maven-mydalgen-plugin](http://git.oschina.net/smartboot/maven-mydalgen-plugin)
- 资源文件：[mydalgen-resource.tar](http://git.oschina.net/smartboot/maven-mydalgen-plugin/attach_files)