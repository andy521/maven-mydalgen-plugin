---
title: table-config.xml配置
category: 用户指南
order: 2
---

mydalgen插件的运行是通过读取配置文件`tables-config.xml`来进行**Mapper XML、DO、DAO**的构建。该xml文件的内容由两个子标签组成`typemap`以及`include`，例如：

    <?xml version="1.0" encoding="UTF-8"?>
    <tables>
        <typemap from="java.sql.Date" to="java.util.Date" />
        <typemap from="java.sql.Timestamp" to="java.util.Date" />
        <typemap from="java.math.BigDecimal" to="java.lang.Long" />
        <typemap from="java.lang.Byte" to="java.lang.Integer" />
        <typemap from="java.lang.Short" to="java.lang.Integer" />
    
        <include table="tables/table1.xml" />
        <include table="tables/table2.xml" />
    </tables>
    
- typemap：	
维护数据库表字段类型与Java类中的变量类型转换关系，该标签只有`from`与`to`两个属性。例如：`<typemap from="java.sql.Date" to="java.util.Date" />`，意味着在构建DO类时，将数据库驱动识别出的`java.sql.Date`类型的表字段，在DO类中定义为`java.util.Date`类型。
- include：
导入mydalgen用于构建代码的模板文件，采用的是相对路径。例如我们的mydalgen资源文件存放于`src/main/resource/META-INF`目录下，且`tables-config.xml`的配置为`<include table="tables/table1.xml" />`，则表示模板文件`table1.xml`是存放于目录`src/main/resource/META-INF/tables`下的。至于模板文件`table1.xml`的编写规则，下一章节继续。