---
title: mydalgen目录结构
category: 用户指南
order: 1
---

通过上一章节大致了解了一个集成了mydalgen插件的工程目录结构为：

    -project-dir  
      +-src/main/resource/META-INF  
      |   +-tables  
	  |   | +-*.xml  
	  |   +-tempates  
	  |   | +-mergedir  
	  |   | +-*.vm
	  |   +-tables-config.xml
      +-pom.xml 
      
> `src/main/resource/META-INF/tables`是由用户创建并用于存放**mydalgen模板文件**的目录。
