---
title: mydalgen模板文件
category: 用户指南
order: 3
---

用户使用mydalgen插件，主要还是在于模板文件的编写，插件再通过模板文件生成MyBatis的`Mapper XML`，`DO类`以及`DAO接口`。

### table
table标签为mydalgen模板文件的根目录标签。		

1. table属性	
	- sqlname	
		必填，源文件对应的数据库表名称
2. table子标签
	- resultMap
	- operation
3. 示例		

	    <?xml version="1.0" encoding="UTF-8"?>
	    <table sqlname="user_info">
			<resultMap **>
				***
			</resultMap>
			<opertation **>
	        	***
			</operation>
	    </table>

### table/resultMap
1. resultMap属性
	- **name**：resultMap的唯一标识
	- **type**：resultMap对应的Java实体类

2. resultMap子标签
	- column  
resultMap对象包含的字段集合，在配置上该标签包含两个属性：
		- **name**：查询语句中的表字段名称
		- **javatype**：字段名对应的Java数据类型
3. 示例

		<resultMap name="USER-INFO"
			type="org.smartboot.smartweb.dal.dataobject.UserInfoDO">
			<column name="id" javatype="int" />
			<column name="password" javatype="java.lang.String" />
		</resultMap>
		
>作者本人对于该标签使用也不频繁，可以使用operation的resultType属性替代

### table/operation
operation为table的子标签，与resultMap标签同级，MyBatis的一条SQL对应于一个opertation标签。mydalgen会根据每一个operation配置生成对应的Mapper配置以及DAO接口。

1. operation属性
	- **name**：必须项，在命名空间中唯一的标识符
	- **multiplicity**：返回SQL执行响应结果数据个数(one/many)，仅`select`操作时该参数有效
	- **paramType**：生成的DAO接口入参类型(object/primitive)。object，构建出的DAO接口入参为当前表对应的DO对象；primitive，构建出的DAO接口入参为operation中SQL语句设置的所有变量。**若operation定义了extraparams子标签,则无需设置该属性**。
	- **resultType**：执行`select`操作从这条语句中返回的期望类型的类的完全限定名或别名。注意如果是集合情形，那应该是集合可以包含的类型，而不能是集合本身。使用 resultType 或 resultMap，但不能同时使用。
	- **resultMap**：外部 resultMap 的命名引用，取值为标签`table/resultMap`的属性`name`值。结果集的映射是 MybatisDalgen 最强大的特性，对其有一个很好的理解的话，许多复杂映射的情形都能迎刃而解。使用 resultMap 或 resultType，但不能同时使用
	
		>resultType、resultMap主要适用于结果集为联表查询的场景，即查询结果的字段来源于不同的表，最终映射到同一个DO对象中。如果查询结果的所有字段皆来自当前模板文件对应数据库表字段，可无需设置resultType、resultMap

2. operation子标签
	- extraparams：自定义DAO接口入参,参数数量、类型、顺序由其子标签param定义。
	- sql：遵循标准JDBC规则编写的SQL语句，生成代码后会成为DAO接口的注释信息
	- sqlmap：遵循Mybatis的`Mapper XML`语法规则定义的SQL语句

### table/operation/extraparams
当前operation的自定义入参集合，extraparams子标签：

- param
定义入参对象，param属性:
	1. **name**：自定义DAO接口参数名称
	2. **javatype**:自定义DAO接口参数类型
