---
title: V1.0.2发布
type: major
---

V1.0.2发布

**发布特性:**

* 处理高版本jdbc驱动包引发的问题
* 精简依赖包
* 去除pom.xml文件的tables属性配置
